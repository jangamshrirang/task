import 'dart:async';
import 'package:flutter/material.dart';
import './model/demo.dart';

void main() {
  runApp(new MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return new MaterialApp(
      home: new HomePage(),
    );
  }
}

class HomePage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      appBar: AppBar(
        title: Text('Groups'),
      ),
      body: Container(
        child: ListView.builder(
            itemCount: group.length,
            itemBuilder: (context, index) {
              return ListTile(
                title: Text(group[index].title),
                onTap: () {
                  Navigator.of(context).push(
                    new MaterialPageRoute(
                        builder: (_) => new MySecondPageBody(
                              name: group[index].name,
                              title: group[index].title,
                            )),
                  );
                },
              );
            }),
      ),
    );
  }
}

class MySecondPageBody extends StatefulWidget {
  final String name;
  final String title;
  MySecondPageBody({this.name, this.title});
  @override
  State createState() => new MySecondPageBodyState();
}

class MySecondPageBodyState extends State<MySecondPageBody> {
  showOverlay() async {
    OverlayState overlayState = Overlay.of(context);
    OverlayEntry overlayEntry = OverlayEntry(
        builder: (context) => Positioned(
            top: MediaQuery.of(context).size.height / 2.0,
            width: MediaQuery.of(context).size.width / 2.0,
            child: Container(
              color: Colors.blue,
              height: 100,
              width: 100,
              child: Center(
                child: Text(
                  'Succefully Join Group ${widget.name}',
                  style: TextStyle(fontSize: 12),
                ),
              ),
            )));

    overlayState.insert(overlayEntry);
    await Future.delayed(Duration(seconds: 2));
    overlayEntry.remove();
  }

  @override
  void initState() {
    showErrorOverlay(context, "${widget.name} join group successfully");

    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(widget.title),
      ),
      body: new Center(
        child: new Text('You are now in ${widget.title} group.'),
      ),
    );
  }
}

showErrorOverlay(BuildContext context, String text) async {
  OverlayState overlayState = Overlay.of(context);
  OverlayEntry overlayEntry =
      OverlayEntry(builder: (context) => _OverlayWidget(text));
  overlayState.insert(overlayEntry);
  Future.delayed(Duration(milliseconds: 1500)).then((_) {
    overlayEntry.remove();
  });
}

class _OverlayWidget extends StatefulWidget {
  final String text;
  const _OverlayWidget(this.text);
  @override
  _OverlayWidgetState createState() => _OverlayWidgetState();
}

class _OverlayWidgetState extends State<_OverlayWidget>
    with TickerProviderStateMixin {
  AnimationController controller;
  Animation<double> animation;
  initState() {
    super.initState();
    controller = AnimationController(
        duration: const Duration(milliseconds: 250), vsync: this);
    animation = CurvedAnimation(parent: controller, curve: Curves.easeIn);
    animation.addStatusListener((status) {
      if (status == AnimationStatus.completed) {
        Future.delayed(Duration(seconds: 4), () {
          if (mounted && controller.isCompleted) {
            controller?.reverse();
          }
        });
      }
    });
    controller.forward();
  }

  @override
  Widget build(BuildContext context) {
    return Positioned(
        top: MediaQuery.of(context).size.height / 2.5,
        left: 16,
        right: 16,
        child: FadeTransition(
            opacity: animation,
            child: GestureDetector(
                onTap: () {
                  controller.reverse();
                },
                child: Material(
                    color: Colors.transparent,
                    child: Opacity(
                      opacity: 0.8,
                      child: Container(
                          padding: EdgeInsets.symmetric(
                              horizontal: 32, vertical: 16),
                          decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(4),
                              color: Colors.red),
                          child: Row(children: <Widget>[
                            Expanded(
                                child: Text(widget.text,
                                    style: TextStyle(color: Colors.white)))
                          ])),
                    )))));
  }

  @override
  void dispose() {
    controller.dispose();
    super.dispose();
  }
}
